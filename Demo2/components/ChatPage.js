import React, { useState, useCallback, useEffect } from "react";
import { GiftedChat } from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {renderBubble, renderTime, renderSender} from "../services/GiftedChatCustom";

//Dette kode er inspireret af et kodeeksempel fra Gifted Chats egen dokumentation

export default function ChatPage ( {route} ) {

    const [messages, setMessages] = useState([]);

    //Min funktion jeg bruger til at hente data ind fra database og oprette en subscriber som overvåger data
    //i real time
    function getMessages() {
        const subscriber = firestore()
        .collection(route.params.roomID)
        .orderBy("postTime", "desc")
        .limit(50) //Denne grænse på 50 var for at arbejde med opgaven om at hente flere beskeder ind når man scroller op men fik ikke lavet det
        .onSnapshot(docs => {
            if (!docs.empty) {
                const items = [];
                docs.forEach(doc => {
                    items.push({
                        _id: doc.id,
                        text: doc.data().messageText,
                        createdAt: doc.data().postTime.toDate(),
                        user: {
                            _id: doc.data().userID,
                            name: doc.data().userName
                        }
                    })
                    setMessages(items);
                })
            }
        })
    }

    useEffect(() => {
        getMessages();
    }, []);

    //Jeg har modificeret den onSend metode som var angivet i dokumentationen
    //og tilføjet et kald til min database med den nye besked
    const onSend = useCallback((messages = []) => {
        setMessages(previousMessages => GiftedChat.append(previousMessages, messages));
        firestore().collection(route.params.roomID).add({
            messageText: messages[0].text,
            postTime: new Date(),
            userID: auth().currentUser.uid,
            userName: auth().currentUser.displayName
        });
    }, []);

    return (
        <GiftedChat
        messages={messages}
        onSend={messages => onSend(messages)}
        user={{
            _id: auth().currentUser.uid,
            name: auth().currentUser.displayName,
            avatar: auth().currentUser.photoURL
        }}
        alwaysShowSend
        renderBubble={renderBubble}
        renderTime={renderTime}
        renderSend={renderSender}
        />
    );
}