import React, {useEffect} from "react";
import LoginPage from "./components/LoginPage";
import ProfilePage from "./components/ProfilePage";
import ChatRooms from "./components/ChatRooms";
import ChatPage from "./components/ChatPage";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import auth from "@react-native-firebase/auth";
import SplashScreen from "react-native-splash-screen";

const {Navigator, Screen} = createStackNavigator();

export default function App() {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  //Tjekker om bruger allerede er logget ind for at bestemme hvilken side den skal loade op først
  if(auth().currentUser){
    return(
    <NavigationContainer>
    <Navigator >
      <Screen name="ChatRooms" component={ChatRooms} options={{headerShown: false}} />
      <Screen name="Profile" component={ProfilePage} options={{headerShown: false}} />
      <Screen name="Login" component={LoginPage} options={{headerShown: false}} />
      <Screen name="ChatPage" component={ChatPage} options={({route}) => ({
        title: route.params.chatRoom
      })}/>
    </Navigator>
  </NavigationContainer>
    )
  }
  return(
    <NavigationContainer>
      <Navigator >
        <Screen name="Login" component={LoginPage} options={{headerShown: false}} />
        <Screen name="Profile" component={ProfilePage} options={{headerShown: false}} />
        <Screen name="ChatRooms" component={ChatRooms} options={{headerShown: false}} />
        <Screen name="ChatPage" component={ChatPage} options={({route}) => ({
        title: route.params.chatRoom
      })}/>
      </Navigator>
    </NavigationContainer>
  );
}