import React, {useState, useEffect} from "react";
import { Text, View, Button, FlatList, StyleSheet, Image, TouchableOpacity } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import Icon from 'react-native-vector-icons/Ionicons';


export default function ChatPage({navigation}) {

  //Da det eneste på denne side der skal opdateres dynamisk er den seneste besked, er det kun det som jeg
  //opretter et state til. 
  const [starWarsMessage, setStarWarsMessage] = useState("");
  const [reactMessage, setReactMessage] = useState("");

  //Jeg laver et lokalt array som jeg kan bruge til at referere til mine chatrum i databasen.
  //Hvis jeg udvidede applikationen med flere chatrum, vil jeg kunne inddrage dem i appen ved blot
  //at indtaste dem i dette array.
  const chatRooms = [
    {
      name: "Star Wars Chat Room",
      roomID: "StarWarsRoom",
      description: "Et chatrum til folk der vil snakke om Star Wars og alting derom",
      latestMessage: starWarsMessage
    },
    {
      name: "React Native Chat Room",
      roomID: "ReactNativeRoom",
      description: "Et chatrum til at snakke om alt vi godt kan lide ved React Native",
      latestMessage: reactMessage
    }
  ]

  //Funktionskald som overvåger seneste besked. Opdateres i Real Time
  async function getLatestMessage(chatRoom) {
    const subscriber = firestore().collection(chatRoom).orderBy("postTime", "desc").limit(1).onSnapshot(docs => {
      if (!docs.empty) { //Dette tjek er nødvendigt for at funktionen ikke giver fejl.
        docs.forEach(doc => {
        if (chatRoom == "StarWarsRoom") {
          setStarWarsMessage(doc.data().messageText)
        } else if (chatRoom == "ReactNativeRoom"){
          setReactMessage(doc.data().messageText)
        }
      })
      }
    })
  }

  //Gennengår mit array af chatrum for at hente den seneste besked
  for (let index = 0; index < chatRooms.length; index++) {
    const room = chatRooms[index];
    getLatestMessage(room.roomID);
  }


  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text style={{marginBottom: 25, marginTop: 25, fontSize: 25, fontWeight: 'bold'}}>Chat Rooms</Text>
      <TouchableOpacity style={styles.button} onPress={() => {
        navigation.navigate("Profile")
      }}>
        <Text style={{color: "white"}}>Profile Page</Text>
      </TouchableOpacity>
      <FlatList data={chatRooms} keyExtractor={item => item.roomID} renderItem={({item}) => (
        <View style={styles.card} >
          <Text style={{fontSize: 25, fontWeight: "bold", textAlign: "center"}}>{item.name}</Text>
          <Text style={{fontSize: 20, fontStyle: "italic", textAlign: "center", color: "#808080", padding: 8}}>"{item.description}"</Text>
          <View style={{flex: 10, flexDirection: "row"}}>
            <View style={{flex:9}}>
              <Text>Seneste besked: {item.latestMessage}</Text>
            </View>
            <View style={{flex:1}}>
              <TouchableOpacity onPress={() => {
                navigation.navigate("ChatPage", {chatRoom: item.name, roomID: item.roomID})
              }}>
                <Icon style={{textAlign: "right", marginTop: -15, color: "#FF3333"}} name="chevron-forward-circle-outline" size={37} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )} />
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center", 
    padding: 10, 
    backgroundColor: "#FF3333",
    marginBottom: 10,
    marginTop: 8,
    width: 150
  },
  card: {
    borderColor: 'black', 
    borderWidth: 1, 
    padding: 9, 
    margin: 5,
    backgroundColor: "white"
  }
});