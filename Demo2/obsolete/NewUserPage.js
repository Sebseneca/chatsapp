import React, { useState } from 'react';
import {View, Text, TextInput, Button, StyleSheet} from 'react-native';
import Authentication from "../components/Authentication";
import createUser from "./CreateUser";
import auth from "@react-native-firebase/auth";

export default function NewUserPage( {navigation} ) {
  const [inputMail, setMail] = useState("");
  const [inputPassword, setPassword] = useState("");
  const [inputName, setName] = useState("");


  return(
    <View style={styles.constainer}>
        <Text style={{marginBottom: 32}}>Create a new user</Text>
        <TextInput 
      style={styles.input}
      value={inputName}
      onChangeText={setName}
      placeholder="Full name"
      />
      <TextInput 
      style={styles.input}
      value={inputMail}
      onChangeText={setMail}
      placeholder="Email Address"
      />
      <TextInput 
      style={styles.input}
      value={inputPassword}
      onChangeText={setPassword}
      secureTextEntry={true}
      placeholder="Password"
      />
      <Button 
      title="Create User" 
      style={styles.input}
      onPress={() => {
          createUser(inputName, inputMail, inputPassword)
      }}/>
    </View>
  );
}

const styles = StyleSheet.create({
  constainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    width: 250,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    marginBottom: 20,
  }
});