import React, { useState, useEffect } from 'react';
import { View, Text, ActivityIndicator, Image } from 'react-native';
import auth from '@react-native-firebase/auth';

//Dette kode var noget jeg brugte tidligt i projektet til at arbejde med login igennem Firebase
//Da det reelt er det jeg bruger til min simple profil side, lader jeg det være. Men det er 
//mest et lævn af tidligere arbejde

export default function Authentication() {
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  if (!user) {
    return (
      <View>
          <ActivityIndicator size="large" color="#0000ff"/>
        <Text>Signing in...</Text>
      </View>
    );
  }

  return (
    <View>
      <Text style={{fontSize: 20, fontWeight: "bold", marginTop: -200, marginBottom: 200}}>Welcome {user.displayName}</Text>
      <Image source={{uri: auth().currentUser.photoURL}} style={{width: 100, height: 100}} />
      <Text>Email Address:  {user.email}</Text>
    </View>
  );
}