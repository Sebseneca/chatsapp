import React from "react";
import {Text, View, StyleSheet, TouchableOpacity} from "react-native";
import Authentication from "../services/Authentication";
import auth from '@react-native-firebase/auth';

//Denne side blev primært brugt til at teste login funktionalitet med Firebase Auth
//for at sikre at det virkede som forventet.

export default function ProfilePage( {navigation} ) {
  return (
    <View style={styles.container}>
      <Authentication />
      <TouchableOpacity style={styles.button} onPress={() => {
      navigation.navigate("ChatRooms")
      }}>
        <Text style={{color: "white"}}>Chat Rooms</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => {
        auth().signOut()
        navigation.navigate("Login")
      }}>
        <Text style={{color: "white"}}>Sign Out</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    alignItems: "center", 
    padding: 10, 
    backgroundColor: "#FF3333",
    marginBottom: 10,
    marginTop: 8,
    width: 150
  }
});