import React from 'react';
import {View, Text, StyleSheet, Alert, Image} from 'react-native';
import auth from '@react-native-firebase/auth';
import {onFacebookButtonPress, onGoogleButtonPress} from "../services/AuthProvider";
import { FacebookSocialButton, GoogleSocialButton } from "react-native-social-buttons";



export default function LoginPage( {navigation} ) {

  //Jeg gør brug af de allerede lavede knapper fra socials buttons pakken og
  //henter logikken til at udføre login fra min Authprovider fil i services

  return(
    <View style={styles.constainer}>
      <View style={styles.up}>
        <Image source={require("../assets/icon.jpg")} style={{marginTop:15}} />
      </View>
      <View style={styles.down} >
      <Text style={{marginBottom: 100, marginTop: -50, fontSize: 25, fontWeight: 'bold'}} >Welcome to ChatsApp</Text>
      <FacebookSocialButton onPress={ async () => {
        try {
          await onFacebookButtonPress()
          navigation.navigate("ChatRooms")
        } catch (error) {
          Alert.alert("Something went wrong", error.message) //Dette virker ikke helt korrekt, den viser ikke fejlbeskeden
        }
      }} />
      <Text ></Text>
      <GoogleSocialButton onPress={ async () => {
        try {
          await onGoogleButtonPress()
          navigation.navigate("ChatRooms")
        } catch (error) {
          Alert.alert("Something went wrong", error.message) //Dette burde dog virke
        }
      }} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  constainer: {
    flex: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  up: {
    flex: 4,
    flexDirection: 'column'
  },
  down: {
    flex: 6,
    flexDirection: 'column'
  }
});