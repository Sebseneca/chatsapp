import React from "react";
import {Text, View } from "react-native";
import { Bubble, Time, Send } from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import Icon from 'react-native-vector-icons/Ionicons';

//Jeg har lavet nogle ændringer til min chat boble, så den skriver brugernavn over beskeden
//og den aligner teksten passende. Jeg sætter boble farverne til hvid og rød for at matche det
//farvetema jeg har i mit logo
export const renderBubble = (props) => {
    if (props.currentMessage.user._id == auth().currentUser.uid) {
        return (
            <View>
                <Text style={{color: "#808080", textAlign: "right"}}>{props.currentMessage.user.name}</Text>
                <Bubble
                    {...props}
                    wrapperStyle={{right: {backgroundColor: "#FF3333"}}}
                />
            </View>
        );
    }
    return (
        <View>
            <Text style={{color: "#808080", textAlign: "left"}}>{props.currentMessage.user.name}</Text>
            <Bubble
                {...props}
                wrapperStyle={{left: {backgroundColor: "#FFFFFF"}}}
            />
        </View>
    );
};

//Jeg laver ligeledes nogle ændringer på tidsteksten i boblen
export const renderTime = (props) => {
    return (
        <Time
        {...props}
        timeTextStyle={{
            left: {
            color: 'black',
            },
        }}
        />
    );
};

export const renderSender = (props) => {
    return (
        <Send {...props}>
            <View>
                <Icon style={{color: "#FF3333", paddingBottom: 5, paddingRight: 5}} name="send-outline" size={35}/>
            </View>
        </Send>
    );
}