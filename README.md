# Chatsapp

_En chat applikation udviklet i React Native med Firebase integration._

Applikationen er udviklet på Windows og det har derfor ikke været muligt at teste iOS delen af applikationen. Alt test har derfor foregået på en Android Emulator eller min personlige Samsung Galaxy S10+.

Appen er testet ved at kalde:

react-native run-android

i rodmappen til projektet. Det er nødvendigt enten at have en Android Emulator kørende når kommandoen køres, eller en Android telefon sat til computeren via USB.

**FOR AT TESTE APPLIKATIONEN**

Der er åbenbart en hulens masse arbejde i at få sat et React Native projekt op til at køre når man henter det ned fra Git. Igennem en masse fejlsøgning har jeg her noteret alle de skridt jeg tog for at kunne køre appen når jeg henter den ned fra Gitlab.

Når projektet er hentet ned, så naviger ind i projektets rodmappe (Demo2) i en terminal.

Herfra skal der køres kommandoen:

npm install --save

Dette installerer alle de pakker der er lokaliseret i projektets package.json fil.

Dette gav mig en fejl med at call stacken var exceeded. Dette kan fikses ved at køre:

npm cache clean --force

Så skal der startes en ny terminal op i rodmappen, hvor der skal køres kommandoen:

npm start -- --reset-cache / npm start / react-native start

Som angiveligt skal køres for at sætte "bundleren" op. Jeg ved ikke om det er nødvendigt, jeg fulgte bare de råd jeg fandt, og slutresultatet var som ønsket.

Herfra forsøgte jeg så at køre react-native run-android men fik en fejl omkring at jeg manglede et Android SDK. Dette løste jeg ved at åbne "android" mappen i mit projekt inde i Android Studio, og så lade det bygge mit projekt. Det var også et problem jeg stødte på tidligt i udviklingen af projektet, og generelt er det mit indtryk at Android Studio hjælper dig med at sætte en masse småting rigtigt op for dig.

Så kunne jeg endelig køre min react-native run-android kommando, og teste applikationen.

**Vigtig info omkring Facebook login** 

For at gøre brug af Facebook login skal man angive en udvikler nøgle for det miljø applikationen bliver kørt i. Som det står beskrevet på Facebooks developer side, skal dette indtastes manuelt for hver miljø appen skal testes i. Da jeg naturligvis kun har kunnet teste applikationen i mine egne miljøer, vil knappen ikke virke korrekt når den bliver kørt i et nyt miljø. 

For at bruge Facebook knappen, er det nødvendigt at jeg kender den hash nøgle der er i det aktuelle miljø. For at finde din nøgle kan du følge anvisningerne på Facebooks side her:
https://developers.facebook.com/docs/facebook-login/android
Under punkt 6 står der beskrevet hvordan man finder sin lokale hash nøgle.

Alternativt, så får du din hash key angivet når du klikker på Facebook knappen i appen. Facebook vil her give dig en fejlbesked, hvor den siger den ikke genkender /denne nøgle/, hvor denne nøgle er din specifikke hash nøgle. Du kan altså blot notere denne nøgle herfra. 

Når du har din hash nøgle, så send den til mig på min mail, seneca13@live.dk, så kan jeg tilføje den til min liste af nøgler som er godkendte. 
